# Spotify Client

Spotify Client for Linux

Download **Spotify-x86_64.AppImage** binary from here: [Spotify-x86_64.AppImage](https://gitlab.com/linuxappimage/spotify/-/jobs/artifacts/master/raw/Spotify-x86_64.AppImage?job=run-build)

or from you command line:

```bash
curl -sLo Spotify-x86_64.AppImage https://gitlab.com/linuxappimage/spotify/-/jobs/artifacts/master/raw/Spotify-x86_64.AppImage?job=run-build

chmod +x Spotify-x86_64.AppImage
./Spotify-x86_64.AppImage

```

## New key

https://www.spotify.com/us/download/linux/

